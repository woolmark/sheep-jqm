Sheep for jQuery Mobile
----------------------------------------------------------------------------
Sheep for jQuery Mobile is an implementation [sheep] for jQuery Mobile.

License
----------------------------------------------------------------------------
[WTFPL] 

[sheep]: https://bitbucket.org/runne/woolmark "Sheep"
[WTFPL]: http://www.wtfpl.net "WTFPL"
